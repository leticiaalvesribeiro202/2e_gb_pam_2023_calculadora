import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@Component({
selector: 'app-home',
templateUrl: 'home.page.html',
styleUrls: ['home.page.scss'],
standalone: true,
imports: [IonicModule, FormsModule],
})
export class HomePage {
visor: string = '0'
numeroGuardado: number = 0
operacaoEscolhida: string = ''
ponto(){}

constructor() {}

escreverNumero(numero: string): void {
if(this.visor === '0'){
this.visor = numero
} else {
this.visor +=numero
}
}

escolherOperacao(operacao: string): void{
switch(operacao){
case '+':
this.numeroGuardado = Number(this.visor)
this.visor = '0'
this.operacaoEscolhida = operacao
break;


case '/':
this.numeroGuardado = Number(this.visor)
this.visor = '0'
this.operacaoEscolhida = operacao
break;

case '-':
  this.numeroGuardado = Number(this.visor)
  this.visor = '0'
  this.operacaoEscolhida = operacao
  break;

  case '*':
this.numeroGuardado = Number(this.visor)
this.visor = '0'
this.operacaoEscolhida = operacao
break;
default:
console.log('Operação não encontrada')
}
}
calcular(): void {
switch(this.operacaoEscolhida){
  case '+':
    this.visor = this.somar().toString()
    break;

    case '/':
      this.visor = this.dividir().toString()
      break;


    case '-':
this.visor = this.subtrair().toString()
break;


case '*':
    this.visor = this.multiplicar().toString()
    break;


default:
console.log('Operação não encontrada')
}
}

somar(): number{
return this.numeroGuardado + Number(this.visor)
}

subtrair(): number{
  return this.numeroGuardado - Number(this.visor)
  }

  multiplicar(): number{
    return this.numeroGuardado * Number(this.visor)
    }

    dividir(): number{
      return this.numeroGuardado / Number(this.visor)
      }

      limpar(){
        this.numeroGuardado = 0;
        this.operacaoEscolhida = '';
        this.visor = ""}

        virgula(){
          if (this.visor.includes(`.`)){

          }else{
            if(this.visor === '0'){

          }else{
            this.visor += `.`;
          }
        }
      }

      deletar() {
        if(this.visor === '0'){
        }else{
          if(this.visor.length == 1){
            this.visor = '0';
          }else{
            this.visor = this.visor.slice(0, this.visor.length - 1);
          }
        }
      }
}